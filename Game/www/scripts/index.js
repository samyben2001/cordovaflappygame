﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    var scoreFlag = true;
    //déclaration objets et methodes
    function Flappy() {
        this.img = "images/flappy.gif";
        this.flappyDiv = $("#flappy");
        this.posY = 300;
        this.posX = 100;
        this.height = 50;
        this.width = 50;
        this.speed = 0;
        this.angle = 0;
        this.alive = true;
    }
    Flappy.prototype.fly = function () {
        this.speed = 550; //px/seconde
        this.posY = this.posY + this.speed * 0.05;
    };
    Flappy.prototype.die = function () {
        $("body").off();
        $("#menu").css({ "display": "block" });

    };
    Flappy.prototype.fall = function (gravity) {
        this.speed = this.speed - gravity * 0.05;
        this.posY = this.posY - this.speed * 0.05;

        //console.log(this.flappyDiv.css("bottom"));
        if (this.posY > window.innerHeight - 97) {
            this.alive = false;
        }
    };
    Flappy.prototype.rotate = function () {
        this.angle = this.speed / -35;
    };
    //gestion collision
    Flappy.prototype.crash = function (pipes, invPipes) {
        //var flappyYtop = (parseInt(this.flappyDiv.css("top")));
        //var flappyYbottom = (parseInt(this.flappyDiv.css("bottom")));
        //var flappyX = (parseInt(this.flappyDiv.css("right")));
        //var pipeY = (parseInt(pipe.pipeDiv.css("top"))) + 400;
        //var pipeX = (parseInt(pipe.pipeDiv.css("right")));
        //var invPipeY = (parseInt(invPipe.invPipeDiv.css("bottom"))) + 400;
        //var invPipeX = (parseInt(invPipe.invPipeDiv.css("right")));

        //console.log("flappyYtop: " + flappyYtop);
        //console.log("flappyYbottom: " + flappyYbottom);
        //console.log("flappyX: " + flappyX);
        //console.log("pipeY: " + pipeY);
        //console.log("pipeX: " + pipeX);
        //console.log("invPipeY: " + invPipeY);
        //console.log("invPipeX: " + invPipeX);
        //return (Math.abs(flappyX - pipeX) < 75) && (Math.abs(flappyYtop - pipeY < 0)) || (Math.abs(flappyX - invPipeX) < 75) && (Math.abs(flappyYbottom - invPipeY < 0))

        var flag = false;
        pipes[0].pipeDiv.each(function () {
            if ($(this).overlaps($("#flappy")).length !== 0) {
                flag = true;
            }
        });
        invPipes[0].invPipeDiv.each(function () {
            if ($(this).overlaps($("#flappy")).length !== 0) {
                this.speed = 50000;
                $("body").trigger("click");
                flag = true;
            }
        });
        return flag;
    };

    function Pipe(posx) {
        this.img = "images/spike2.png";
        this.pipeDiv = $(".pipe");
        this.posY;
        this.posX = posx;
        this.height;
        this.width;
        this.speed = 30;
    }

    Pipe.prototype.move = function () {
        this.posX += 5;
        if (this.posX >= 500) {
            this.posX = -100;
            scoreFlag = true;
            return true;
        }
    };

    function InversePipe(posx) {
        this.img = "images/spike.png";
        this.invPipeDiv = $(".invPipe");
        this.posY;
        this.posX = posx;
        this.height;
        this.width;
        this.speed = 30;
    }
    InversePipe.prototype.move = function () {
        this.posX += 5;
        if (this.posX >= 500) {
            this.posX = -100;
            scoreFlag = true;
            return true;
        }
    };

    function Game(level) {
        this.background = "images/Bg.png";
        this.backgroundDiv;
        this.level = level;
        this.gravity = 1050;
        this.score = 0;
        this.scoreDiv = $("#score");
        this.flappy;
        this.pipes = [];
        this.inversePipes = [];
    }

    Game.prototype.updatePipesPos = function (index) {
        var random = Math.random();
        this.pipes[index].posY = -250 + 100 * random;
        this.inversePipes[index].posY = 350 + 100 * random;
    };

    //initialistaion du jeu
    Game.prototype.start = function () {
        var pipe1 = $('#pipe1');
        var pipe2 = $('#pipe2');
        var invPipe1 = $('#invPipe1');
        var invPipe2 = $('#invPipe2');
        this.pipes = [new Pipe(-100), new Pipe(-350)];
        this.inversePipes = [new InversePipe(-100), new InversePipe(-350)];

        this.updatePipesPos(0);
        this.updatePipesPos(1);
        pipe1.css({ "right": this.pipes[0].posX, "top": this.pipes[0].posY, "background-image": "url(" + this.pipes[0].img + ")" });
        pipe2.css({ "right": this.pipes[1].posX, "top": this.pipes[1].posY, "background-image": "url(" + this.pipes[1].img + ")" });
        invPipe1.css({ "right": this.inversePipes[0].posX, "top": this.inversePipes[0].posY, "background-image": "url(" + this.inversePipes[0].img + ")" });
        invPipe2.css({ "right": this.inversePipes[1].posX, "top": this.inversePipes[1].posY, "background-image": "url(" + this.inversePipes[1].img + ")" });

        this.backgroundDiv = $("body");
        this.backgroundDiv.css({ "background-image": "url(" + this.background + ")" });

        this.flappy = new Flappy();
        this.flappy.flappyDiv.css({
            "top": this.flappy.posY,
            "left": this.flappy.posX,
            "background-image": "url(" + this.flappy.img + ")"
        });
    };

    //rafraichissement du jeu
    Game.prototype.update = function () {

        //mouvement du background
        var left = parseInt(this.backgroundDiv.css("background-position-x")) + 2;
        this.backgroundDiv.css("background-position-x", left + "%");
        //mouvement du flappy
        this.flappy.fall(this.gravity);
        this.flappy.rotate();
        this.flappy.flappyDiv.css({
            "top": this.flappy.posY,
            "transform": "rotate(" + this.flappy.angle + "deg)"
        });
        //mouvement des pipes
        for (var i = 0; i < 2; i++) {
            this.pipes[i].move();

            if (this.inversePipes[i].move()) {
                this.updatePipesPos(i);
            }

            $("#pipe" + (i + 1)).css({
                "right": this.pipes[i].posX + "px",
                "top": this.pipes[i].posY + "px"
            });
            $("#invPipe" + (i + 1)).css({
                "right": this.inversePipes[i].posX + "px",
                "top": this.inversePipes[i].posY + "px"
            });

            //Score
            var flappyR = parseInt($("#flappy").css("right"));
            if (flappyR < this.inversePipes[i].posX && scoreFlag) {
                this.score++;
                scoreFlag = false;
                this.scoreDiv.html(this.score);
            }
        }

    };

    function onDeviceReady() {
        var game = new Game();
        var menu = $("#menu");
        var endScore = $("#endScore");
        game.start();

        var interval = window.setInterval(function () {
            if (!game.flappy.alive) {
                clearInterval(interval);
                menu.css({ "display": "block" });
                endScore.html("Score: " + game.score);
            }
            game.update();
            //flappy die si collision
            if (game.flappy.crash(game.pipes, game.inversePipes)) {
                game.gravity = 5000;
                game.flappy.die();
            }

        }, 50);

        $("#restart").click(function () {
            window.location.reload();
            menu.css({ "display": "none" });
        });

        $('body').click(function () {
            game.flappy.fly();
            game.flappy.rotate();
            game.flappy.flappyDiv.css({
                "top": game.flappy.posY,
                "transform": "rotate(" + game.flappy.angle + "deg)"
            });
        });
    }
})();